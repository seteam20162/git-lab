unit MatrixU;

interface

uses
  Crt;

const
  Size = 8;
  ColorOnSD = 14;
  ColorUnderSD = 9;

type
  TypeOfMatrix = array [1..Size, 1..Size] of Integer;

var
  i: integer;
  RowWithMinOnSecDiagonal, ColumnWithMinOnSecDiagonal: Integer;
  RowWithMinUnderSecDiagonal, ColumnWithMinUnderSecDiagonal: Integer;

procedure FillMinElement(color: Integer);

procedure FindMinElementOnSD(var matrix: TypeOfMatrix; var minOnSD, numRowMinOnSD, numColumnMinOnSD: integer);

procedure FindMinElementUnderSD(var matrix: TypeOfMatrix; var minUnderSD, numRowMinUnderSD, numColumnMinUnderSD: integer);

procedure CreateMatrix(var matrix: TypeOfMatrix);

procedure WriteMatrix(var matrix: TypeOfMatrix);

procedure WriteElement(var matrix: TypeOfMatrix; NumRow, NumColumn: Integer);

procedure ReturnInformationAboutMinElement(location: string; element, row, column: Integer);

procedure ReplaceByIndexInMatrix(RowOfRecipient, ColumnOfRecipient, RowOfSource, ColumnOfSource: Integer; var matrix: TypeOfMatrix);

implementation

procedure FillMinElement(color: Integer);
begin
  textColor(color);
end;

procedure FindMinElementOnSD(var matrix: TypeOfMatrix; var minOnSD, numRowMinOnSD, numColumnMinOnSD: integer);
var
  i, j: Integer;
begin
  minOnSD := matrix[Size, 1];
  numRowMinOnSD := Size;
  numColumnMinOnSD := 1;
  RowWithMinOnSecDiagonal := Size;
  ColumnWithMinOnSecDiagonal := 1;
  for i := 1 to Size do
  begin
    j := (Size - i + 1);
    if (matrix[i, j] < minOnSD) then
    begin
      minOnSD := matrix[i, j];
      RowWithMinOnSecDiagonal := i;
      ColumnWithMinOnSecDiagonal := j;
      numRowMinOnSD := i;
      numColumnMinOnSD := j;
    end;
  end;
end;

procedure FindMinElementUnderSD(var matrix: TypeOfMatrix; var minUnderSD, numRowMinUnderSD, numColumnMinUnderSD: integer);
var
  i, j: Integer;
begin
  minUnderSD := matrix[Size, Size];
  numRowMinUnderSD := Size;
  numColumnMinUnderSD := Size;
  RowWithMinUnderSecDiagonal := Size;
  ColumnWithMinUnderSecDiagonal := Size;
  for i := 2 to Size do
  begin
    for j := Size - i + 2 to Size do
    begin
      if (matrix[i, j] < minUnderSD) then
      begin
        minUnderSD := matrix[i, j];
        RowWithMinUnderSecDiagonal := i;
        ColumnWithMinUnderSecDiagonal := j;
        numRowMinUnderSD := i;
        numColumnMinUnderSD := j;
      end;
    end;
  end;
end;

procedure CreateMatrix(var matrix: TypeOfMatrix);
const
  Min = -11;
  Max = 9;
var
  i, j: Integer;
begin
  Randomize;
  for i := 1 to Size do
  begin
    for j := 1 to Size do
    begin
      matrix[i, j] := Min + random(Max - Min);
    end;
  end;
end;

procedure WriteElement(var matrix: TypeOfMatrix; NumRow, NumColumn: Integer);
begin
  textColor(15);
  if (NumRow = RowWithMinOnSecDiagonal) and (NumColumn = ColumnWithMinOnSecDiagonal) then
    FillMinElement(ColorOnSD);
  if (NumRow = RowWithMinUnderSecDiagonal) and (NumColumn = ColumnWithMinUnderSecDiagonal) then
    FillMinElement(ColorUnderSD);
  Write(matrix[NumRow, NumColumn]:5);
  textColor(15);
end;

procedure WriteMatrix(var matrix: TypeOfMatrix);
var
  i, j: Integer;
begin
  for i := 1 to Size do
  begin
    for j := 1 to Size do
    begin
      WriteElement(matrix, i, j);
    end;
    WriteLn;
  end;
end;

procedure ReturnInformationAboutMinElement(location: string; element, row, column: Integer);

begin
  WriteLn('The minimum element ', location, ' the secondary diagonal x[', row, ',', column, ']= ', element);
end;

procedure ReplaceByIndexInMatrix(RowOfRecipient, ColumnOfRecipient, RowOfSource, ColumnOfSource: Integer;
var matrix: TypeOfMatrix);
var
  TempColor: Integer;
begin
  matrix[RowOfRecipient, ColumnOfRecipient] := matrix[RowOfSource, ColumnOfSource];
end;

begin
  RowWithMinOnSecDiagonal := 0;
  ColumnWithMinOnSecDiagonal := 0;
  RowWithMinUnderSecDiagonal := 0;
  ColumnWithMinUnderSecDiagonal := 0;
end.