program LR_OPI;

uses
  MatrixU;

var
  MinOnSecDiagonal, RowMinOnSecDiagonal, ColumnMinOnSecDiagonal: Integer;
  MinUnderSecDiagonal, RowMinUnderSecDiagonal, ColumnMinUnderSecDiagonal: Integer;
  Matrix: TypeOfMatrix;

begin
  CreateMatrix(Matrix);
  FindMinElementOnSD(Matrix, MinOnSecDiagonal, RowMinOnSecDiagonal, ColumnMinOnSecDiagonal);
  FindMinElementUnderSD(Matrix, MinUnderSecDiagonal, RowMinUnderSecDiagonal, ColumnMinUnderSecDiagonal);
  WriteLn();
  WriteMatrix(Matrix);
  WriteLn;
  ReturnInformationAboutMinElement('On', MinOnSecDiagonal, RowMinOnSecDiagonal, ColumnMinOnSecDiagonal);
  WriteLn();
  ReturnInformationAboutMinElement('Under', MinUnderSecDiagonal, RowMinUnderSecDiagonal, ColumnMinUnderSecDiagonal);
  ReplaceByIndexInMatrix(RowMinOnSecDiagonal, ColumnMinOnSecDiagonal, RowMinUnderSecDiagonal, ColumnMinUnderSecDiagonal, Matrix);
  WriteLn();
  WriteMatrix(Matrix);
  ReadLn;
end.